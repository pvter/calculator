package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    private static final Map<String, String> errors = new HashMap<>() {{
        put("EMPTY", "пустое выражение.");
        put("INVALID", "некорректное выражение.");
        put("BRACKET", "в выражении лишняя скобка.");
        put("OPERATOR", "недопустимый оператор: ");
        put("DIVISION", "недопустимый делитель");
    }};

    public static void main(String[] args) {

        try {
            String inputExpression = readInput();
            validateInput(inputExpression);

            String outputExpression = convertToRPN(inputExpression);
            double result = calcResult(outputExpression);
            String formattedResult = formatResult(result);

            printOutput(outputExpression);
            printResult(formattedResult);
        } catch (Exception e) {
            print("Ошибка: " + e.getMessage());
        }

        print("\n");
        main(args);
    }

    /**
     * Ввод арифметического выражения
     */
    private static String readInput() throws IOException {
        print("Введите арифметическое выражение");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }

    /**
     * Проверка арифметического выражения на валидность
     */
    private static void validateInput(String expression) throws Exception {

        if (expression.isEmpty()) throw new Exception(errors.get("EMPTY"));

        AtomicInteger i = new AtomicInteger();
        Stream.of(expression.split("")).forEach(ch -> {
            int currentIdx = i.get();
            char currentChar = ch.charAt(0);
            Character prevChar = null;
            Character nextChar = null;

            if (currentIdx > 0) prevChar = expression.charAt(currentIdx - 1);
            if (currentIdx < expression.length() - 1) nextChar = expression.charAt(currentIdx + 1);

            try {
                if (!isDigit(currentChar) && !isOperator(currentChar) && !isValidChar(currentChar)) {
                    String message = getInvalidCharMessage(currentChar);
                    throw new Exception(message);
                }

                validateChar(currentChar, prevChar, nextChar);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }

            i.incrementAndGet();
        });
    }

    private static String getInvalidCharMessage(Character ch) {
        String message = "недопустимый символ: ";
        if (ch == ' ') message += "Пробел";
        else message += ch;
        message += "\nВыражение может содержать только числа, операторы +,-,*,/, скобки и десятичный разделитель(.)";
        return message;
    }

    private static void validateChar(Character currentChar, Character prevChar, Character nextChar) throws Exception {
        switch (currentChar) {
            case '.':
                if (prevChar == null || !isDigit(prevChar)) throw new Exception(errors.get("INVALID"));
                if (nextChar == null || !isDigit(nextChar)) throw new Exception(errors.get("INVALID"));
                break;
            case '(':
                if (prevChar != null && !isOperator(prevChar) && prevChar != '(')
                    throw new Exception(errors.get("INVALID"));
                if (nextChar != null && !isDigit(nextChar) && nextChar != '(' && nextChar != '-' && nextChar != '+')
                    throw new Exception(errors.get("INVALID"));
                break;
            case ')':
                if (prevChar != null && !isDigit(prevChar) && prevChar != ')')
                    throw new Exception(errors.get("INVALID"));
                if (nextChar != null && !isOperator(nextChar) && nextChar != ')')
                    throw new Exception(errors.get("INVALID"));
                break;
        }

        if (isOperator(currentChar)) {
            if (prevChar != null && isOperator(prevChar)) throw new Exception(errors.get("INVALID"));
            if (nextChar != null && isOperator(nextChar)) throw new Exception(errors.get("INVALID"));
        }
    }

    private static boolean isDigit(char ch) {
        return Character.isDigit(ch);
    }

    private static boolean isOperator(char ch) {
        List<Character> operators = Arrays.asList('+', '-', '−', '*', '/');
        return operators.contains(ch);
    }

    private static boolean isValidChar(char ch) {
        List<Character> validChars = Arrays.asList('.', '(', ')');
        return validChars.contains(ch);
    }

    /**
     * Преобразование выражения в формат обратной польской нотации
     */
    private static String convertToRPN(String expression) throws Exception {
        StringBuilder output = new StringBuilder();
        Stack<Character> stack = new Stack<>();

        // Пока в исходной строке есть необработанные лексемы, считываем очередную:
        for (int i = 0; i < expression.length(); i++) {
            char currentChar = expression.charAt(i);

            if (isDigit(currentChar) || currentChar == '.') { // Если лексема — число, добавляем в строку вывода.
                output.append(currentChar);
            } else if (currentChar == '(') { // Если лексема — открывающая скобка, помещаем в стек.
                stack.push(currentChar);
            } else if (isOperator(currentChar)) { // Если лексема — Оператор O1.
                int currentCharPriority = getPriority(currentChar);

                // кодируем унарный минус отдельным символом
                if (currentChar == '-' && i > 0 && expression.charAt(i - 1) == '(') currentChar = '−';

                // Пока присутствует на вершине стека лексема-оператор (O2), чей приоритет выше или равен приоритету O1,
                // перекладываем O2 из стека в выходную очередь.
                while (!stack.isEmpty() && isOperator(stack.peek())) {
                    char lastStackItem = stack.peek();
                    int lastStackItemPriority = getPriority(lastStackItem);
                    if (lastStackItemPriority >= currentCharPriority) {
                        output.append(' ');
                        output.append(stack.pop());
                    } else break;
                }

                // Помещаем O1 в стек.
                stack.push(currentChar);

            } else if (currentChar == ')') {  // Если лексема — закрывающая скобка
                // Пока лексема на вершине стека не станет открывающей скобкой, перекладываем лексемы-операторы из стека в выходную очередь.
                while (!stack.isEmpty() && stack.peek() != '(') {
                    output.append(' ');
                    output.append(stack.pop());
                }
                if (!stack.isEmpty() && stack.peek() == '(') stack.pop(); // Удаляем из стека открывающую скобку.
                else
                    throw new Exception(errors.get("BRACKET") + " Символ номер " + i); // Если стек закончился до того, встретилась открывающая скобка — в выражении содержится ошибка.
            }

            if (isOperator(currentChar)) output.append(' ');
        }

        // Если во входной строке больше не осталось лексем, пока есть операторы в стеке -
        // перекладываем оператор из стека в выходную очередь.
        while (!stack.isEmpty()) {
            char lastItem = stack.pop();
            // Если на вершине стека скобка — в выражении допущена ошибка.
            if (lastItem == '(' || lastItem == ')') throw new Exception(errors.get("BRACKET"));
            output.append(' ');
            output.append(lastItem);
        }

        return output.toString().trim();
    }

    /**
     * Получение приоритета оператора
     */
    private static int getPriority(char c) throws Exception {
        switch (c) {
            case '*':
            case '/':
                return 1;
            case '+':
            case '-':
            case '−':
                return 0;
        }
        throw new Exception(errors.get("OPERATOR") + c);
    }

    /**
     * Вычисление значения выражения, заданного в формате обратной польской нотации
     */
    private static double calcResult(String expression) throws Exception {
        List<String> items = new ArrayList<>(List.of(expression.split(" "))).stream()
                .filter(item -> item.length() > 0).collect(Collectors.toList());

        double result = 0.0;

        if (items.isEmpty()) throw new Exception(errors.get("EMPTY"));
        if (items.size() == 1) {
            if (!isNumber(items.get(0))) throw new Exception(errors.get("INVALID"));
            return Double.parseDouble(items.get(0));
        }

        while (!items.isEmpty()) {
            OptionalInt optPos = IntStream.range(0, items.size())
                    .filter(i -> items.get(i).length() == 1 && isOperator(items.get(i).charAt(0))).findFirst();
            if (optPos.isEmpty()) break;

            int operatorPosition = optPos.getAsInt();
            if (operatorPosition == 0 || items.get(operatorPosition - 1).isEmpty())
                throw new Exception(errors.get("INVALID"));

            char o = items.get(operatorPosition).charAt(0);
            double y = Double.parseDouble(items.get(operatorPosition - 1));
            double x = operatorPosition == 1 ? 0 : Double.parseDouble(items.get(operatorPosition - 2));
            result = performOperation(o, x, y);

            if (o == '−') {
                items.set(operatorPosition - 1, Double.toString(result));
                items.remove(operatorPosition);
                continue;
            }

            if (operatorPosition > 1) {
                items.set(operatorPosition - 2, Double.toString(result));
                items.remove(operatorPosition);
                items.remove(operatorPosition - 1);
            } else {
                items.set(0, Double.toString(result));
                items.remove(operatorPosition);
            }

        }

        return result;
    }

    /**
     * Проверка возможности преобразования строки в число
     */
    private static boolean isNumber(String str) {
        if (str == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Выполнение арифметического действия
     */
    private static double performOperation(char operator, double x, double y) throws Exception {
        switch (operator) {
            case '+':
                return x + y;
            case '-':
                return x - y;
            case '−':
                return -y;
            case '*':
                return x * y;
            case '/':
                if (y == 0) throw new Exception(errors.get("DIVISION"));
                return x / y;
        }
        throw new Exception(errors.get("OPERATOR") + operator);
    }

    /**
     * Форматирование вывода результата вычисления
     */
    private static String formatResult(double number) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.UK);
        DecimalFormat format = (DecimalFormat) numberFormat;
        format.setMaximumFractionDigits(10);
        return format.format(number);
    }

    /**
     * Вывод выражения в консоль
     */
    private static void printOutput(String outputExpression) {
        print("Выражение в обратной польской нотации:");
        print(outputExpression);
    }

    /**
     * Вывод результата вычислений в консоль
     */
    private static void printResult(String result) {
        print("Результат вычисления:");
        print(result);
    }

    private static void print(String str) {
        System.out.println(str);
    }
}
